import 'dart:math';

import 'package:zoysii/src/util/utils.dart';
import 'package:zoysii/src/util/local_data_controller.dart';
import 'package:zoysii/src/resources/levels.dart';
import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/screens/game/util/player.dart';
import 'package:zoysii/src/screens/game/util/random.dart';

// Class to manage in-game variables and methods
class Match {
  Match() {
    init();
  }

  // Match id
  // In levels mode id equals to: -(selectedLevel[0] * 7 + selectedLevel[1] + 1)
  int id;

  // The match continues until isWin == null
  // If playerOne won: playerOneWon == true
  // If playerOne lose: playerOneWon == false
  bool playerOneWon;

  // Match status
  bool isPause = false;

  // Starting match date, saved as milliseconds since epoch
  int initDate;

  // Match grid. It represents a square of integers
  List<int> board;

  // Match grid size length. It equals to sqrt(board)
  int sideLength;

  // Remaining tiles to be deleted to win
  int toBeDeleted;

  // List of active players
  List<Player> playerList;

  // Max numbers of moves allowed in levels (levels mode only variable)
  int maxMoves;

  // Selected level to play (levels mode only variables)
  List<int> selectedLevel;

  // Level name (levels mode only variables)
  String levelName;

  // Initialize match. This method must be called before every match
  void init() {
    initDate = DateTime.now().millisecondsSinceEpoch;
    playerOneWon = null;
    isPause = false;
    playerOne = Player(position: 0);
    playerList = [playerOne];
    if (mode != 0) {
      id = setRandom(id);
      sideLength = settings.sideLength;
      board = List.generate(sideLength * sideLength,
          (index) => getRandom(settings.minBoardInt, settings.maxBoardInt));
      if (mode == 2) {
        playerTwo = CpuPlayer(board.length - 1);
        playerList.add(playerTwo);
      }
      toBeDeleted = sideLength * (sideLength - 1) - 1;
    } else {
      selectedLevel ??= [level[0], level[1]];
      id = -(selectedLevel[0] * 7 + (selectedLevel[1] + 1));
      for (final level in levels) {
        if (listEquals(level['level'], selectedLevel)) {
          levelName = level['name'];
          maxMoves = level['max_moves'];
          board = []..addAll(level['grid']);
          break;
        }
      }
      sideLength = sqrt(board.length).toInt();
    }
  }

  // Check if someone won the match
  // It returns true if playerOne won, false if playerOne lose, null otherwise
  bool someoneWon() {
    int zeros = board.where((tile) => tile == 0).length;
    toBeDeleted = sideLength * (sideLength - 1) - zeros - 1;
    if (mode == 0) {
      if (zeros == board.length)
        playerOneWon = playerOne.moves <= maxMoves ? _levelWon() : false;
    } else if (toBeDeleted <= 0) {
      toBeDeleted = 0;
      if (mode == 1) _saveData();
      playerOneWon = playerOne.points > (playerTwo?.points ?? -1);
    } else if (mode == 2) {
      List<Player> aliveList =
          playerList.where((player) => player.alive).toList();
      playerOneWon =
          aliveList.length == 1 ? playerOne == aliveList.first : null;
    }
    return playerOneWon;
  }

  // Calculate next level and save data. _levelWon() always return true
  bool _levelWon() {
    if (listEquals(selectedLevel, nextLevel) && nextLevel[1] < 6)
      ++nextLevel[1];
    else if (listEquals(selectedLevel, nextLevel) && nextLevel[1] == 6) {
      ++nextLevel[0];
      nextLevel[1] = 0;
    }
    _saveData();
    return true;
  }

  // Save won match data in results
  void _saveData() {
    saveResults(
        initDate,
        DateTime.now().millisecondsSinceEpoch,
        id,
        sideLength,
        playerOne.moves,
        playerOne.points,
        settings.minBoardInt,
        settings.maxBoardInt,
        settings.biggestLow);
    if (mode == 0) saveNextLevel();
  }
}
