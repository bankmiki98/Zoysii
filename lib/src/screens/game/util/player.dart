import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/screens/game/util/random.dart';

// Player managed by user
Player playerOne = Player();

// Player managed by CPU in multi-player matches
CpuPlayer playerTwo;

// Class to manage players
class Player {
  Player({this.position});

  // Starting position
  int position = 0;

  // Moves count
  int moves = 0;

  // Earned points
  int points = 1;

  // Player Status
  bool alive = true;

  // Function to call when the player has been deleted
  void deleted() {
    alive = false;
  }

  // Execute move
  void doMove(int direction, double distance) {
    if (match.isPause) return;

    int previousPosition = position;
    int del = 0;
    if (direction == 0) {
      if (distance > 0) {
        // Right
        if ((previousPosition + 1) % match.sideLength != 0) {
          ++position;
          ++moves;
          for (int index = position; index % match.sideLength != 0; index++) {
            del += _hitTile(index, previousPosition) ? 1 : 0;
          }
        }
      } else if (distance < 0) {
        // Left
        if (previousPosition % match.sideLength != 0) {
          --position;
          ++moves;
          for (int index = position;
              (index + 1) % match.sideLength != 0;
              index--) {
            del += _hitTile(index, previousPosition) ? 1 : 0;
          }
        }
      }
    } else if (direction == 1) {
      if (distance > 0) {
        // Down
        if (previousPosition + match.sideLength < match.board.length) {
          position += match.sideLength;
          ++moves;
          for (int index = position;
              index < match.board.length;
              index += match.sideLength) {
            del += _hitTile(index, previousPosition) ? 1 : 0;
          }
        }
      } else if (distance < 0) {
        // Up
        if (previousPosition > match.sideLength - 1) {
          position -= match.sideLength;
          ++moves;
          for (int index = position; index >= 0; index -= match.sideLength) {
            del += _hitTile(index, previousPosition) ? 1 : 0;
          }
        }
      }
    }
    // If one ore more tiles have been deleted, delete starting tile too
    if (del > 0) {
      match.board[previousPosition] = 0;
    }
  }

  // Redefine grid values and acquire points
  bool _hitTile(int index, int previousPosition) {
    if (match.board[index] == 0) return false;
    bool deletedTile = false;
    int previousValue = match.board[index];
    match.board[index] -= match.board[previousPosition];
    if (match.board[index] == 0) {
      points += 4 * match.board[previousPosition];
      match.playerList.forEach((player) {
        if (player != this && player.position == index && mode == 2) {
          player.deleted();
        }
      });
      deletedTile = true;
    }
    if (match.board[index] < 0 && match.board[previousPosition] != 0) {
      match.board[index] = match.board[index].abs();
    }
    if (List.generate(mode == 0 ? 2 : settings.biggestLow, (i) => i + 1)
        .contains(match.board[index])) {
      match.board[index] = previousValue + match.board[previousPosition];
    }
    return deletedTile;
  }
}

// Class to manage non-human players
class CpuPlayer extends Player {
  CpuPlayer(int position) : super(position: position);
  // Calculate CPU direction and execute move
  void cpuMove() {
    int direction = 0;
    double distance = 0;

    for (int j = position; j % match.sideLength != 0; j++) {
      if (match.board[j] == match.board[position] &&
          j != position &&
          match.board[position] != 0) {
        // Right
        direction = 0;
        distance = 1;
      }
    }
    for (int j = position; (j + 1) % match.sideLength != 0; j--) {
      if (match.board[j] == match.board[position] &&
          j != position &&
          match.board[position] != 0) {
        // Left
        direction = 0;
        distance = -1;
      }
    }
    for (int j = position; j < match.board.length; j += match.sideLength) {
      if (match.board[j] == match.board[position] &&
          j != position &&
          match.board[position] != 0) {
        // Down
        direction = 1;
        distance = 1;
      }
    }
    for (int j = position; j >= 0; j -= match.sideLength) {
      if (match.board[j] == match.board[position] &&
          j != position &&
          match.board[position] != 0) {
        // Up
        direction = 1;
        distance = -1;
      }
    }
    if (direction == 0 && distance == 0) {
      if (playerOne.position < position) {
        // Left or Up
        direction = getRandom(0, 1);
        distance = -1;
      }
      if (playerOne.position > position) {
        // Right or Down
        direction = getRandom(0, 1);
        distance = 1;
      }
      if (playerOne.position == position) {
        if (position < (match.board.length / 2).round()) {
          // Right or Down
          direction = getRandom(0, 1);
          distance = 1;
        } else {
          // Left or Up
          direction = getRandom(0, 1);
          distance = -1;
        }
      }
    }
    // Execute move
    doMove(direction, distance);
  }
}
