import 'dart:math';

// Random int generator
int currentSeed;
Random _currentRandom;

// Return random int between min and max
int getRandom(int min, int max) => min + _currentRandom.nextInt(max - min + 1);

int setRandom(int inputSeed) {
  Random base = Random();
  currentSeed = (inputSeed ?? -1) == -1 ? base.nextInt(9999999) : inputSeed;
  _currentRandom = Random(currentSeed);
  return currentSeed; // Used as Match.id
}
