import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/resources/levels.dart';
import 'package:zoysii/src/screens/info/util/licenses_utils.dart';
import 'package:zoysii/src/screens/info/resources/info_menu_list.dart';

class InfoPage extends StatefulWidget {
  @override
  _InfoPageState createState() => _InfoPageState();
}

class _InfoPageState extends State<InfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('I N F O'),
        centerTitle: true,
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(8.0),
          itemCount: infoMenuList.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: Icon(
                infoMenuList[index]['icon'].icon,
                size: 27,
              ),
              title: Text(
                infoMenuList[index]['title'],
                style: TextStyle(
                  fontSize: kTextFontSize,
                ),
              ),
              subtitle: Text(infoMenuList[index]['subtitle']),
              onTap: () {
                if (infoMenuList[index]['url'].length != 0)
                  launch('${infoMenuList[index]['url']}');
                else if (infoMenuList[index]['title'] == 'Third Party Licenses')
                  _licenseDialog();
                else if (++debugClickCount == 17) nextLevel = [zones.length, 0];
              },
            );
          }),
    );
  }

  // Display third party licenses in an alert dialog
  void _licenseDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Third Party Licenses'),
          content: SingleChildScrollView(
              child: Container(child: Text(licensesText()))),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // Counter used to enter in debug mode
  int debugClickCount = 0;
}
