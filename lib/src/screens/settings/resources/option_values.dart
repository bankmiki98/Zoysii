// List of possible grid sizes
List<int> gridSizes = [5, 6, 7, 8, 9, 10];

// List of possible biggest "low numbers"
List<int> lowNumberList = Iterable<int>.generate(6).toList();

// List of possible input methods
Map<int, String> inputMethods = {
  0: 'Gestures',
  1: 'Gamepad',
};

// List of possible virtual gamepad sizes
Map<int, String> gamepadSizes = {
  30: 'Smallest',
  40: 'Small',
  50: 'Default',
  60: 'Large',
  70: 'Largest',
};

// List of possible gamepad shapes
Map<int, String> gamepadShape = {
  0: 'Classic (＋)',
  1: 'WASD (⊥)',
};

// List of possible gamepad shapes
Map<int, String> numberRepresentation = {
  0: 'Arabic',
  1: 'Hex',
  2: 'Letters',
  3: 'Aegean',
  4: 'Armenian',
  5: 'Khmer',
//  6: 'Farsi',
//  7: 'Burmese',
//  8: 'Roman',
//  9: 'Chinese',
};
