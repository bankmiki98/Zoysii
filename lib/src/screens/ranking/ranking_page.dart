import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/ranking/util/list_utils.dart';

class RankingPage extends StatefulWidget {
  RankingPage({Key key}) : super(key: key);

  @override
  _RankingPageState createState() => _RankingPageState();
}

class _RankingPageState extends State<RankingPage> {
  @override
  void initState() {
    super.initState();
    setState(() {
      filter = settings.sideLength;
    });
  }

  @override
  Widget build(BuildContext context) {
    filterList(filter);
    sortList(sorter);

    return Scaffold(
      appBar: AppBar(
          title: Text('Ranking (${filter}x$filter)'),
          actions: _popupMenuButtons()),
      body: Scrollbar(
        child: shownResults.length == 0
            ? Center(child: Text('Nothing to show'))
            : ListView.builder(
                itemCount: shownResults.length,
                itemBuilder: (context, index) => ListTile(
                  leading: Text(
                    '${index + 1}',
                    style: TextStyle(
                      fontSize: kTextFontSize - 2,
                      foreground: Paint()..color = Colors.red[400],
                    ),
                  ),
                  title: Text(
                    'ID: ${shownResults[index][2]}',
                    style: TextStyle(
                      fontSize: kTextFontSize,
                    ),
                  ),
                  trailing: Text(
                    '${shownResults[index][4]} moves; ${shownResults[index][5]} points',
                    style: TextStyle(
                      fontSize: kTextFontSize - 4,
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  // Return popup menu buttons used to filter and order shown results
  List<Widget> _popupMenuButtons() {
    return [
      PopupMenuButton<int>(
          icon: Icon(Icons.sort),
          tooltip: 'Sort',
          onSelected: (int sortChoice) {
            setState(() {
              sorter = sortChoice;
            });
          },
          itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>[
                const PopupMenuItem<int>(
                  value: 1,
                  child: Text('Moves'),
                ),
                const PopupMenuItem<int>(
                  value: 2,
                  child: Text('Points'),
                ),
              ]),
      PopupMenuButton<int>(
        icon: Icon(Icons.filter_list),
        tooltip: 'Filter',
        onSelected: (int filterChoice) {
          setState(() {
            filter = filterChoice;
          });
        },
        itemBuilder: (BuildContext context) {
          List<PopupMenuEntry<int>> popupEntries = [];
          for (int i = 5; i <= 10; i++) {
            popupEntries.add(PopupMenuItem<int>(
              value: i,
              child: Text('${i}x$i'),
            ));
          }
          return popupEntries;
        },
      )
    ];
  }
}
