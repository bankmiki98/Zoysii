import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/game/game_page.dart';
import 'package:zoysii/src/screens/home/resources/page_list.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Z O Y S I I'),
        centerTitle: true,
        backgroundColor: Colors.blueGrey[800],
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(8.0),
          itemCount: pageList.length,
          itemBuilder: (BuildContext context, int index) {
            if (pageList[index].containsKey('divider'))
              return Divider(color: Colors.black45);
            else
              return ListTile(
                leading: Icon(
                  pageList[index]['icon'].icon,
                  size: 27,
                ),
                title: Text(
                  pageList[index]['title'],
                  style: TextStyle(
                    fontSize: kTextFontSize,
                  ),
                ),
                subtitle: Text(pageList[index]['subtitle']),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      mode = pageList[index]['gameMode'];
                      return pageList[index]['page'];
                    }),
                  );
                },
              );
          }),
    );
  }
}
