// List of rules of the game
final List<Map<String, dynamic>> rules = [
  {
    'number': '1',
    'title': 'Who am I?',
    'subtitle': 'You are the red tile on a square board',
  },
  {
    'number': '2',
    'title': 'How do I move?',
    'subtitle': 'Swipe horizzontally or vertically to move',
  },
  {
    'number': '3',
    'title': 'What happens after my move?',
    'subtitle':
    'When you move you reduce tiles value in the direction you are going',
  },
  {
    'number': '3a',
    'title': 'How much does this value decrease?',
    'subtitle':
    'The amount of this reduction is equal to your starting point tile value',
  },
  {
    'number': '3b',
    'title': 'What happens to low numbers?',
    'subtitle':
    'If the value of a tile would be equal to 1 or 2, there will be an increase instead of a decrease',
  },
  {
    'number': '3c',
    'title': 'What if a number becomes lower than zero?',
    'subtitle': 'Negative numbers become positive (absolute value)',
  },
  {
    'number': '3d',
    'title': 'What if a number becomes equal to zero?',
    'subtitle':
    'If the value of a tile becomes equal to zero, starting tile value becomes zero too. Tiles have been "Deleted"',
  },
  {
    'number': '4',
    'title': 'How do I score points?',
    'subtitle': 'Deleted tiles cause an increase of your points',
  },
  {
    'number': '5',
    'title': 'What is the aim?',
    'subtitle':
    'The aim in single and multiplayer modes is to delete almost every tile [n(n-1)-1] while trying to make the most points',
  },
  {
    'number': '6',
    'title': 'What if I delete my opponent?',
    'subtitle':
    "In multiplayer matches a player can win by deleting opponent's tile",
  },
];