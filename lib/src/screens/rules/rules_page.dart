import 'package:flutter/material.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/rules/resources/rules.dart';

class RulesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('R U L E S'),
        centerTitle: true,
      ),
      body: ListView.separated(
          padding: const EdgeInsets.all(4.0),
          itemCount: rules.length,
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: CircleAvatar(
                backgroundColor: Colors.blueGrey[500],
                child: Text(
                  rules[index]['number'],
                  style: TextStyle(
                    fontSize: kTextFontSize,
                    color: Colors.white,
                  ),
                ),
              ),
              title: Text(
                rules[index]['title'],
                style: TextStyle(
                  fontSize: kTextFontSize - 2,
                ),
              ),
              subtitle: Text(
                rules[index]['subtitle'],
                style: TextStyle(
                  fontSize: kTextFontSize - 4,
                ),
              ),
              //onTap: () {},
            );
          }),
    );
  }
}
