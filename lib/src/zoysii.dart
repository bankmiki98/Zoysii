import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/home/home_page.dart';
import 'package:zoysii/src/util/local_data_controller.dart';

class Zoysii extends StatefulWidget {
  static _ZoysiiState of(BuildContext context) =>
      context.ancestorStateOfType(const TypeMatcher<_ZoysiiState>());

  @override
  _ZoysiiState createState() => _ZoysiiState();
}

class _ZoysiiState extends State<Zoysii> {
  @override
  void initState() {
    super.initState();
    loadStoredData().whenComplete(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: 'Zoysii',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
        primaryColor: Colors.blueGrey[800],
        brightness: settings.darkTheme ? Brightness.dark : Brightness.light,
      ),
      home: HomePage(),
    );
  }
}
