// List compare
bool listEquals<T>(List<T> a, List<T> b) {
  if (a == null) return b == null;
  if (b == null || a.length != b.length) return false;
  for (int index = 0; index < a.length; ++index) {
    if (a[index] != b[index]) return false;
  }
  return true;
}
