import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:zoysii/src/resources/global_variables.dart';
import 'package:zoysii/src/screens/settings/resources/settings_class.dart';

SharedPreferences _prefs;

// Import all data from shared preferences
Future<void> loadStoredData() async {
  _prefs = await SharedPreferences.getInstance();
  _loadResults().then((x) => results = x);
  _loadSettings();
  _loadNextLevel();
}

// Save results in shared preferences
void saveResults(int initDate, int endDate, int matchId, int sideLength,
    int moves, int points, minBoardInt, maxBoardInt, biggestLow) async {
  results.add([
    initDate,
    endDate,
    matchId,
    sideLength,
    moves,
    points,
    minBoardInt,
    maxBoardInt,
    biggestLow
  ]);
  _prefs.setString('results', jsonEncode(results));
}

// Restore results
Future<List<List<int>>> _loadResults() async {
  List<List<int>> _results = List<List<int>>();
  try {
    List<dynamic> stringList =
        (jsonDecode(_prefs.getString('results')) as List<dynamic>)
            .cast<List<dynamic>>();
    stringList.forEach((x) => _results.add(x.cast<int>()));
  } on NoSuchMethodError {
    // Empty list
  }
  return Future<List<List<int>>>(() => _results);
}

// Save app settings in shared preferences
void saveSettings() async {
  _prefs.setString('settings', jsonEncode(settings.toJson()));
}

void saveGamepadOffset() async {
  _prefs.setDouble('gamepadOffsetX', kGamepadOffset.dx);
  _prefs.setDouble('gamepadOffsetY', kGamepadOffset.dy);
}

// Restore app settings
void _loadSettings() async {
  settings = Settings(jsonDecode(_prefs.getString('settings') ?? '{}'));
  kGamepadOffset = Offset(_prefs.getDouble('gamepadOffsetX') ?? 0,
      _prefs.getDouble('gamepadOffsetY') ?? 0);
}

// Save next level to play in shared preferences
void saveNextLevel() async {
  _prefs.setInt('lastWorldSolved', nextLevel[0]);
  _prefs.setInt('lastLevelSolved', nextLevel[1]);
}

// Restore next level to play
void _loadNextLevel() async {
  nextLevel[0] = _prefs.getInt('lastWorldSolved') ?? 0;
  nextLevel[1] = _prefs.getInt('lastLevelSolved') ?? 0;
}
