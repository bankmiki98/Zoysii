// Converts input number into selected notation
String intToSelectedNotation(int inputNumber, int notation) {
  if (inputNumber == 0) return '•';
  String sign = '';
  if (inputNumber < 0) {
    inputNumber = inputNumber.abs();
    sign = '-';
  }
  switch (notation) {
    case 0:
      return sign + inputNumber.toString();
      break;
    case 1:
      return sign + inputNumber.toRadixString(16).toUpperCase();
      break;
    case 2:
      return sign + intToLetter(inputNumber);
      break;
    case 3:
      return sign + intToAegean(inputNumber);
      break;
    case 4:
      return sign + intToArmenian(inputNumber);
      break;
    case 5:
      return sign + intToKhmer(inputNumber);
      break;
    default:
      return ':(';
  }
}

// Map numeral system to font family
Map<int, String> notationFontMap = {
  0: '',
  1: '',
  2: '',
  3: 'NotoSansLinearB',
  4: 'NotoSansArmenian',
  5: 'NotoSansKhmer',
};

// Convert int into roman number (0 <= input <= 999)
String intToRoman(int input) {
  if (input == 0) return '0';
  List<String> C = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM'];
  List<String> X = ['', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC'];
  List<String> I = ['', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX'];
  return C[((input % 1000) ~/ 100)] + X[((input % 100) ~/ 10)] + I[input % 10];
}

// Convert int into its letter representation (0 < input <= 702)
// A == 1, B == 2, Z == 26, AA == 27, AB == 28, ...
String intToLetter(int input) {
  const String alphabet = ' ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  int intDivide = input ~/ 26;
  int remainder = input % 26;
  return (alphabet[intDivide - (remainder != 0 ? 0 : 1)] +
      alphabet[remainder != 0 ? remainder : 26])
      .trim();
}

// Convert int into Aegean
String intToAegean(int input) {
  if (input == 0) return '0';
  List<String> C = ['', '𐄙', '𐄚', '𐄛', '𐄜', '𐄝', '𐄞', '𐄟', '𐄠', '𐄡'];
  List<String> X = ['', '𐄐', '𐄑', '𐄒', '𐄓', '𐄔', '𐄕', '𐄖', '𐄗', '𐄘'];
  List<String> I = ['', '𐄇', '𐄈', '𐄉', '𐄊', '𐄋', '𐄌', '𐄍', '𐄎', '𐄏'];
  return C[((input % 1000) ~/ 100)] + X[((input % 100) ~/ 10)] + I[input % 10];
}

// Convert int into Armenian
String intToArmenian(int input) {
  if (input == 0) return '0';
  List<String> C = ['', 'Ճ', 'Մ', 'Յ', 'Ն', 'Շ', 'Ո', 'Չ', 'Պ', 'Ջ'];
  List<String> X = ['', 'Ժ', 'Ի', 'Լ', 'Խ', 'Ծ', 'Կ', 'Հ', 'Ձ', 'Ղ'];
  List<String> I = ['', 'Ա', 'Բ', 'Գ', 'Դ', 'Ե', 'Զ', 'Է', 'Ը', 'Թ'];
  return C[((input % 1000) ~/ 100)] + X[((input % 100) ~/ 10)] + I[input % 10];
}

// Convert int into chinese number (0 <= input <= 999)
String intToChinese(int input) {
  String inputString = input.toString();
  String output = '';
  List<String> units = ['', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
  List<String> dec = ['', '十', '百'];
  for (int i = 0; i < inputString.length; i++) {
    if (inputString[i] == '1' && i == 0 && i < inputString.length - 1) {
      output += dec[inputString.length - 1];
      continue;
    }
    output += units[int.parse(inputString[i])] + dec[inputString.length - 1 - i];
  }
  return output;
}

// Convert int into Khmer
String intToKhmer(int input) {
  const String khmer = '០១២៣៤៥៦៧៨៩';
  String output = '';
  input.toString().split('').forEach((digit) {
    output += khmer[int.parse(digit)];
  });
  return output;
}

// Convert int into Farsi
String intToFarsi(int input) {
  const String farsi = '۰۱۲۳۴۵۶۷۸۹';
  String output = '';
  input.toString().split('').forEach((digit) {
    output += farsi[int.parse(digit)];
  });
  return output;
}

// Convert int into Burmese
String intToBurmese(int input) {
  const String burmese = '၀၁၂၃၄၅၆၇၈၉';
  String output = '';
  input.toString().split('').forEach((digit) {
    output += burmese[int.parse(digit)];
  });
  return output;
}